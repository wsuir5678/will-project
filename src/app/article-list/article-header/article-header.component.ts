import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  signal,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Article } from '../article.interface';

@Component({
  selector: 'app-article-header',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './article-header.component.html',
  styleUrls: ['./article-header.component.css'],
})
export class ArticleHeaderComponent implements OnInit, OnChanges {

  @Input() item!: Article;

  @Output() delete = new EventEmitter<any>();
  @Output() changeTitle = new EventEmitter<any>();

  isEdit = false;
  newTitle = signal<string>('');
  origin_item!: Article;
  articleID: any;

  constructor() {}

  ngOnInit() {
    // this.newTitle.set(this.item.title);
  }

  ngOnChanges( {item}: SimpleChanges):void {
    if (item) {
      this.origin_item = item.currentValue;
      this.item = Object.assign({}, this.origin_item);
    }
  }

  doDeleteArticle() {
    this.delete.emit(this.item);
  }

  doChangeTitle() {
    this.articleID = {
      id: this.item.id,
      title:this.item.title
    }
    this.changeTitle.emit(this.articleID);
    this.isEdit = false;
  }

  onCanel() {
    this.item = Object.assign({}, this.origin_item);
    this.isEdit = false;
  }
}

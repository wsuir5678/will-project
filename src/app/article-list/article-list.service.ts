import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Article, NewTitle } from './article.interface';
import { Observable, lastValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ArticleListService {
  url = 'http://localhost:3000/articles'
  http = inject(HttpClient);

  constructor() {}

  getArticles = async() : Promise<Article[]> => {
    const result$ = this.http.get<Article[]>(`${this.url}`)
    return await lastValueFrom(result$)
  }

  removeArticle = async(id:number) =>{
    const result$ = this.http.delete(`${this.url}/${id}`);
    return await lastValueFrom(result$)
  }

  modifyArticle = async(newTitle:NewTitle) => {
    const result$ = this.http.patch(`${this.url}/${newTitle.id}`, newTitle as NewTitle);
    return await lastValueFrom(result$)
  }

}

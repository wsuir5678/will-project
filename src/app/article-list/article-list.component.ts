import { Component, OnInit, WritableSignal, inject, signal } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HttpClientModule } from '@angular/common/http';
import { Article } from './article.interface';
import { ArticleHeaderComponent } from './article-header/article-header.component';
import { ArticleBodyComponent } from './article-body/article-body.component';
import { ArticleListService } from './article-list.service';

@Component({
  selector: 'app-article-list',
  standalone: true,
  imports: [
    CommonModule,
    ArticleHeaderComponent,
    ArticleBodyComponent,
    HttpClientModule,
  ],
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.css'],
  providers: [ArticleListService],
})
export class ArticleListComponent implements OnInit {
  articleService = inject(ArticleListService);
  // items = signal([] as Article[]);
  articles : WritableSignal<Article[]> = signal([]);
  public counter = 0;

  constructor() {}
  async ngOnInit(): Promise<void> {
    this.articles.set(await this.articleService.getArticles());
  }

  async onDeleteArtivle(article: Article) {
    try {
      await this.articleService.removeArticle(article.id);
      this.articles.mutate(i => {
        i.splice(i.findIndex(i => i.id === article.id),1);
      });
    } catch(error) {
      console.error(error)
    }
  }

  async onModifyArticle(article:Article){
    try{
      await this.articleService.modifyArticle(article);
      this.articles.mutate(i => {
        i[i.findIndex(i => i.id === article.id)].title = article.title;
      })
    }catch (error){
      console.error(error)
    }
  }


}

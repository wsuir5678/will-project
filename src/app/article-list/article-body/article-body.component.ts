import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-article-body',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './article-body.component.html',
  styleUrls: ['./article-body.component.css']
})
export class ArticleBodyComponent implements OnInit,OnChanges{

    @Input() item:any;

    @Input() counter:number | undefined ;

    constructor(){
      // console.log("ArticleBodyComponent:constructor")
    }

    ngOnInit(): void {
      // console.log(`ArticleBodyComponent ${this.item.id}:OnInit`)

    }

    ngOnChanges(changes:any){
      // console.log(`ArticleBodyComponent ${this.item.id}:OnChanges`)
      // console.log(changes)
    }
}

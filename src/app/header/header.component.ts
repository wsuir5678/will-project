import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent {
  public title = 'will-project';
  public url = 'http://blog.miniasp.com/';
  public imgUrl = '/assets/images/logo.png';
  public counter = 0;

  changeTitle(altKey: boolean) {
    if (altKey) {
      this.title = 'New Homepage';
    }
    this.counter++;
  }
}
